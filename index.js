const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/user');
//lets acquire the route component for course
const courseRoutes = require('./routes/course')

require('dotenv').config()

const portNumber = process.env.PORT;
const connectionString = process.env.DB_CONNECTION_STRING;

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.get('/', function(req, res){
	res.send('Capstone Server hosted properly online')
})

app.listen(portNumber || 4000, () => {
	console.log(`API is now online on port ${portNumber||4000}`)
});

mongoose.connection.once('open', () => console.log('Now  connected to MongoDB Atlas.'));
mongoose.connect(connectionString, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use(cors());

app.use('/api/user', userRoutes);
app.use('/api/course', courseRoutes);