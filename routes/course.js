const express = require('express');
const router = express.Router();
//lets acquire the controller module for our course
const CourseController = require('../controllers/course');
// const auth = require('../auth');


router.post('/exists', (req, res) => {
	CourseController.exists(req.body).then(result => res.send(result))
});
//lets create our first endpoint that will allow us to send request to "create" a new course inside the database.
router.post('/create', (req, res) => {
  //will describe the next set of procedures that will happen upon sending the request in the specified endpoint.
  //upon sending the request to the enpoint you are already instatntiating a promise. 
  CourseController.insert(req.body).then(result => res.send(result)); 
})

router.get('/', (req, res) =>{
	CourseController.getAll().then(courses => res.send(courses))
})

router.get('/:id', (req, res) => {
     let courseId = req.params.id
     CourseController.get({ courseId }).then(courses => res.send(courses))
})

module.exports = router;