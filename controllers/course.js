const User = require('../models/user');
const Course = require('../models/course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//[Primary Section]//there are the requirements for capstone 2

//1. create a function that will allow us to create a new course inside our database
module.exports.insert = (params) => {
    let course = new Course({
    	name: params.name,
    	description: params.description,
    	price: params.price
    })
    //once you have successfully captured the data inserted by the user, its now time to save it inside the database 
    //by applying the save() in the course variable we are already creating a promise object. 
    return course.save().then((course, err) => {
        return (err) ? false : true
    })
}

module.exports.exists = (params) => {
	return Course.find({name: params.name }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.getAll = (params) => {
	// upon doing this query inside our collection we are instantiating a promise
	return Course.find({isActive: true}).then(courses => courses)
}

module.exports.get = (params) => {
//we need to run a query on the course collection to identify which course to display.
   return Course.findById(params.courseId).then(courses => courses)
}
